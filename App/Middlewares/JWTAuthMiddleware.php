<?php
namespace App\Middlewares;

use App\Core\Middleware;
use App\Core\Request;
use App\Util\JWTUtil;
use App\Util\JWTAuth;
use App\Util\Response;

class JWTAuthMiddleware extends Middleware {
    use JWTUtil ,JWTAuth;

    /*
    *
    Authenticate users by JSON Web Tokens
    *
    */
    public function handle(Request $request)
    {
        $this->setJWTHeaders();
        if(is_null($request->token) || !$this->isValidToken($request->token)) {
            $this->logger->error($request->ip . 'user could not be granted : JWTAuth');
            Response::returnJSONResponse([
                'success' => false,
                'auth'    => true,
                'message' => 'user could not be granted'
            ]);
            die();
        }
    }
}