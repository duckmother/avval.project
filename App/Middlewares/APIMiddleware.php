<?php
namespace App\Middlewares;

use App\Core\Middleware;
use App\Core\Request;

class APIMiddleware extends Middleware {

    /*
    *
    JSON encode all requests
    *
    */
    public function handle(Request $request) {
        if (is_object($request)) {
            $request->params = json_encode($request->params);
        }
    }
}