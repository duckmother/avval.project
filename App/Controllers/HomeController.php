<?php
namespace App\Controllers;

use App\Core\Controller;
use App\Core\Request;
use App\Models\Post;

class HomeController extends Controller {

    protected function middleware(): array
    {
        return ['JWTAuth'];
    }

    public function index(Request $request) {
        //
    }
}