<?php
namespace App\Controllers;

use App\Core\Controller;
use App\Core\Request;
use App\Models\Post;
use App\Models\Comment;
use App\Util\Response;
use App\Util\JWTAuth;
use App\Models\User;

class CommentController extends Controller {
    use JWTAuth;

    protected function middleware(): array
    {
        return ['API'];
    }

    /*
    *
    * Returns a list of comments related to a given post_id
    *
    */
    public function index(Request $request) {
        if(is_null($request->id)) {
            $this->logger->error($request->ip . 'can not show comments without comments\' post id on CommentController:index()');
            return Response::returnJSONResponse([
                'success' => false,
                'message' => 'please enter post id'
            ]);
        }
    
        $post = new Post();
        $post = $post->find($request->id);
        if(empty((array)$post)) {
            $this->logger->error($request->ip . 'could not find requested post on CommentController:index()');
            return Response::returnJSONResponse([
                'success' => false,
                'message' => 'can not find post'
            ]);
        }

        $comments = new Comment();
        $comments = $comments->where([
            'post_id' => $request->id
        ]);

        if(!empty((array)$comments)) {
            return Response::returnJSONResponse([
                'success' => true,
                'message' => $comments
            ]);
        }
        $this->logger->error($request->ip . 'no comments for this post yet CommentController:index()');
        return Response::returnJSONResponse([
            'success' => false,
            'message' => 'no comments for this post yet'
        ]);
    }

    /*
    *
    * Creates a new post comment related to a given post_id
    *
    */
    public function store(Request $request) {
        if(is_null($request->post_id)) {
            $this->logger->error($request->ip . 'can not create comment without comment\'s post id on CommentController:store()');
            return Response::returnJSONResponse([
                'success' => false,
                'message' => 'please enter post id'
            ]);
        }

        $post = new Post();
        $post = $post->find($request->post_id);
        if(empty((array)$post)) {
            $this->logger->error($request->ip . 'could not find requested post on CommentController:store()');
            return Response::returnJSONResponse([
                'success' => false,
                'message' => 'can not find post'
            ]);
        }

        $validator = $this->validator($request , [
            'body' => 'required|min:3|max:2000',
            'post_id' => 'required|integer',
        ]);
        if(!$validator) {
            $this->logger->error($request->ip . 'comment credentials are not complete in CommentController:store():' . implode(',' , $this->errors()));
            return Response::returnJSONResponse([
                'success' => false,
                'message' => $this->errors()
            ]);
        }
        $comment = new Comment();
        if($comment->create([
            'body' => $request->body,
            'user_id' => $this->getUserWithToken($request->token)->id,
            'post_id' => $request->post_id
        ])) {
            return Response::returnJSONResponse([
                'success' => true
            ]);
        }
        $this->logger->error($request->ip . 'can not create comment on CommentController:store()');
        return Response::returnJSONResponse([
            'success' => false,
            'message' => 'comment cannot be created'
        ]);
    }

    /*
    *
    * Deletes a comment with a given comment_id. note that just these people
    * can delete the comment:
    * someone who authors the post that comment is for that
    * someone who sends the comment
    *
    */
    public function destroy(Request $request) {
        if(is_null($request->id)) {
            $this->logger->error($request->ip . 'comment id is not given CommentController:destroy()');
            return Response::returnJSONResponse([
                'success' => false,
                'message' => 'please send a comment id to delete comment'
            ]);
        }
        
        $comment = new Comment();
        $cComment = $comment->find($request->id);

        if(empty((array)$cComment)) {
            $this->logger->error($request->ip . 'could not find requested comment on CommentController:destroy()');
            return Response::returnJSONResponse([
                'success' => false,
                'message' => 'can not find comment'
            ]);
        }


        if($cComment->user_id != $this->getUserWithToken($request->token)->id &&
        $this->getUserWithToken($request->token)->id != $this->getCommentPostAuthor($cComment->post_id)) {
            $this->logger->error($request->ip . 'user is not authorized to delete comment on CommentController:destroy()');
            return Response::returnJSONResponse([
                'success' => false,
                'message' => 'you are not authorized to delete comment'
            ]);
        }

        if($comment->delete($request->id)) {
            return Response::returnJSONResponse([
                'success' => true
            ]);
        }
        $this->logger->error($request->ip . 'can not delete comment on CommentController:destroy()');
        return Response::returnJSONResponse([
            'success' => false,
            'message' => 'comment cannot be deleted'
        ]);
    }

    /*
    *
    * Gets a post author id
    *
    */
    public function getCommentPostAuthor(int $post_id) {
        $post = new Post();
        $post = $post->find($post_id);
        return $post->user_id;
    }
}