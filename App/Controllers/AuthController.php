<?php
namespace App\Controllers;

use App\Core\Controller;
use App\Core\Request;
use App\Models\User;
use App\Util\Response;
use App\Util\JWTAuth;

class AuthController extends Controller {
    use JWTAuth;

    protected function middleware(): array
    {
        return ['API'];
    }

    /*
    *
    * Register users with name, email, phone and password
    * if LOGIN_AFTER_REGISTERATION is set to true, it will login
    * users after registeration
    *
    */
    public function register(Request $request) {
        $user = new User();
        $password = password_hash($request->password , PASSWORD_BCRYPT);
        if(!$this->validator($request , [
            'name' => 'required|min:3|max:255',
            'email' => 'required|mail|max:255',
            'phone' => 'required|integer',
            'password' => 'required|min:8|max:255'
        ])) {
            $this->logger->error($request->ip . 'user credentials are not complete in AuthController:register():' . implode(',' , $this->errors()));
            return Response::returnJSONResponse([
                'success' => false,
                'message' => $this->errors()
            ]);
        }
        if($user->create([
            'name'     => $request->name,
            'email'    => $request->email,
            'phone'    => $request->phone,
            'password' => $password,
            'ip'       => $request->ip
        ])) {
            Response::returnJSONResponse([
                'success' => true
            ]);
            if(LOGIN_AFTER_REGISTERATION)
                return $this->loginAfterRegisteration($request->email , $request->password);
            return redirect('/');
        }
        $this->logger->error($request->ip . 'can not create user on UserController:register()');
        return Response::returnJSONResponse([
            'success' => false,
            'message' => 'user can not be registerated'
        ]);
    }

    /*
    *
    * If LOGIN_AFTER_REGISTERATION is set to true, it will login
    * users after registeration
    *
    */
    public function loginAfterRegisteration(string $email , string $password) {
        $user = $this->isValidUser($email , $password);
        if (!empty((array)$user)) {
            $jwt = $this->createToken($user);
            return Response::returnJSONResponse([
                'success' => true,
                'token'   => $jwt
            ]);
        }
        $this->logger->error('can not login user on UserController:loginAfterRegisteration()');
        return Response::returnJSONResponse([
            'success' => false,
            'auth'    => true,
            'message' => 'cannot login user'
        ]);
    }

    /*
    *
    * Logins users with email and password and returns a valid jwt token
    *
    */
    public function login(Request $request) {
        if (is_null($request->email) && is_null($request->password)) {
            $this->logger->error($request->ip . 'can not login user without an email and a password on UserController:login()');
            return Response::returnJSONResponse([
                'success' => false,
                'message' => 'please enter email and password'
            ]);
        }
        $user = $this->isValidUser($request->email , $request->password);
        if (!empty((array)$user)) {
            $jwt = $this->createToken($user);
            return Response::returnJSONResponse([
                'success' => true,
                'token'   => $jwt
            ]);
        }
        $this->logger->error($request->ip . 'can not login user on UserController:login()');
        return Response::returnJSONResponse([
            'success' => false,
            'auth'    => true,
            'message' => 'cannot login user'
        ]);
    }

    /*
    *
    * Gets user data by a valid token
    *
    */
    public function getAuthUser(Request $request) {
        if(!is_null($request->token)) {
            $user = $this->getUserWithToken($request->token);
            if(!empty((array)$user)) {
                return Response::returnJSONResponse([
                    'success' => true,
                    'user' => $user
                ]);
            }
        }
        $this->logger->error($request->ip . 'can not find user credentials on UserController:getAuthUser()');
        return Response::returnJSONResponse([
            'success' => false,
            'auth'    => true,
            'message' => 'user could not be granted'
        ]);
        
    }
}