<?php
namespace App\Controllers;

use App\Core\Controller;
use App\Core\Request;
use App\Models\Post;
use App\Util\Response;
use App\Util\JWTAuth;

class PostController extends Controller {
    use JWTAuth;

    protected function middleware(): array
    {
        return ['API'];
    }

    /*
    *
    * Returns list of all posts
    *
    */
    public function index(Request $request) {
        $posts = new Post();
        $posts = $posts->all();
        if(!empty((array)$posts)) {
            return Response::returnJSONResponse([
                'success' => true,
                'message' => $posts
            ]);
        }
        $this->logger->error($request->ip . 'no posts yet in PostController:index()');
        return Response::returnJSONResponse([
            'success' => false,
            'message' => 'no posts yet'
        ]);

    }

    /*
    *
    * Returns a post related to a given id. if id is not given or 
    * there is no post with the given id, it will return all posts
    *
    */
    public function show(Request $request) {
        if(!is_null($request->id)) {
            $post = new Post();
            $post = $post->find($request->id);
            if(!empty((array)$post)) {
                return Response::returnJSONResponse([
                    'success' => true,
                    'message' => $post
                ]);
            }
        }
        return $this->index($request);
    }

    /*
    *
    * Creates a new post with title and body
    *
    */
    public function store(Request $request) {
        $validator = $this->validator($request , [
            'title' => 'required|min:3|max:255',
            'body' => 'required|min:3',
        ]);
        if(!$validator) {
            $this->logger->error($request->ip . 'post credentials are not complete in PostController:store():' . implode(',' , $this->errors()));
            return Response::returnJSONResponse([
                'success' => false,
                'message' => $this->errors()
            ]);
        }
        $post = new Post();
        if($post->create([
            'title' => $request->title,
            'body' => $request->body,
            'user_id' => $this->getUserWithToken($request->token)->id
        ])) {
            return Response::returnJSONResponse([
                'success' => true
            ]);
        }
        $this->logger->error($request->ip . 'can not create post on PostController:store()');
        return Response::returnJSONResponse([
            'success' => false,
            'message' => 'post cannot be created'
        ]);
    }

    /*
    *
    * Updates a post related to a given id. Note that only author of a 
    * post can update it.
    *
    */
    public function update(Request $request) {
        if(is_null($request->id)) {
            $this->logger->error($request->ip . 'post id is not given PostController:update()');
            return Response::returnJSONResponse([
                'success' => false,
                'message' => 'please send a post id to update'
            ]);
        }
        
        $post = new Post();
        $cPost = $post->find($request->id);
        
        if(empty((array)$cPost)) {
            $this->logger->error($request->ip . 'could not find requested post on PostController:update()');
            return Response::returnJSONResponse([
                'success' => false,
                'message' => 'can not find post'
            ]);
        }

        if($cPost->user_id != $this->getUserWithToken($request->token)->id) {
            $this->logger->error($request->ip . 'user is not authorized to update post on PostController:update()');
            return Response::returnJSONResponse([
                'success' => false,
                'message' => 'you are not authorized to update post'
            ]);
        }

        $validator = $this->validator($request , [
            'title' => 'required|min:3|max:255',
            'body' => 'required|min:3',
        ]);
        if(!$validator) {
            $this->logger->error($request->ip . 'post credentials are not complete in PostController:update():' . implode(',' , $this->errors()));
            return Response::returnJSONResponse([
                'success' => false,
                'message' => $this->errors()
            ]);
        }
        
        if($post->update([
            'title' => $request->title,
            'body' => $request->body,
        ] , [
            'id' => $request->id
        ])) {
            return Response::returnJSONResponse([
                'success' => true
            ]);
        }
        $this->logger->error($request->ip . 'can not update post on PostController:update()');
        return Response::returnJSONResponse([
            'success' => false,
            'message' => 'post cannot be updated'
        ]);
    }

    /*
    *
    * Deletes a post related to a given id. Note that only author of a 
    * post can delete it.
    *
    */
    public function destroy(Request $request) {
        if(is_null($request->id)) {
            $this->logger->error($request->ip . 'post id is not given PostController:destroy()');
            return Response::returnJSONResponse([
                'success' => false,
                'message' => 'please send a post id to delete'
            ]);
        }

        $post = new Post();
        $cPost = $post->find($request->id);

        if(empty((array)$cPost)) {
            $this->logger->error($request->ip . 'could not find requested post on PostController:destroy()');
            return Response::returnJSONResponse([
                'success' => false,
                'message' => 'can not find post'
            ]);
        }

        if($cPost->user_id != $this->getUserWithToken($request->token)->id) {
            $this->logger->error($request->ip . 'user is not authorized to delete post on PostController:destroy()');
            return Response::returnJSONResponse([
                'success' => false,
                'message' => 'you are not authorized to delete post'
            ]);
        }


        if($post->delete($request->id)) {
            return Response::returnJSONResponse([
                'success' => true
            ]);
        }
        $this->logger->error($request->ip . 'can not delete post on PostController:destroy()');
        return Response::returnJSONResponse([
            'success' => false,
            'message' => 'post cannot be deleted'
        ]);
    }
}