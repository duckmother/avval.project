<?php
namespace App\Services\Router;

use App\Core\Request;
use App\Util\RouteUtil;

class Router {
    use RouteUtil;

    public static $routes;
    public static $apiRoutes;
    public const BaseControllerNamespace = '\\App\Controllers\\';
    public const BaseMiddlewareNamespace = '\\App\Middlewares\\';
    public static $routeCredentials;

    /*
    *
    Checks if a route doesexist. if yes, call its appropriate controller
    * method
    *
    */
    public static function bootstrap() : void {
        self::$routes    = include ROUTE_PATH . 'web.php';
        self::$apiRoutes = include ROUTE_PATH . 'api.php';
        $currentRoute = self::getCurrentRoute();
        $requestMethod = strtolower($_SERVER['REQUEST_METHOD']);

        if(self::isRoute($currentRoute)) {
            self::$routeCredentials = self::getRouteCredentials($currentRoute ,
             self::$routes , $requestMethod);
        }
        elseif(self::isAPIRoute($currentRoute)) {
            self::$routeCredentials = self::getRouteCredentials($currentRoute ,
             self::$apiRoutes , $requestMethod);
        }
        else {
            self::send404();
        }
        if(empty((array)self::$routeCredentials)) {
            self::send403();
        }
        else {
            self::callControllerMethod();
        }
    }

    /*
    *
    gets controller class, controller method and middlewares of a given 
    * route and calls them
    *
    */
    private static function callControllerMethod() : void {
        list($controllerName , $methodName) = explode(ROUTE_DELIMITER , self::$routeCredentials->dest);

        $controllerNamespace = self::BaseControllerNamespace  . $controllerName;
        if(class_exists($controllerNamespace)) {
            $controller = new $controllerNamespace;
            $request = new Request();
            self::callControllerMiddlewares($controller , $request);
            self::callRouteMiddlewares($request);
            $controller->$methodName($request);
        }
        else {
            self::send404();
        }
    }

    /*
    *
    Call controller middlewares that are specified in middleware function 
    * of controller
    *
    */
    private static function callControllerMiddlewares(object $controller , Request $request) : void{
        if (is_countable($controller->middlewares)) {
            foreach($controller->middlewares as $middleware) {
                self::callMiddleware($middleware , $request);
            }
        }
    }

    /*
    *
    * Call route middlewares that are specified in route middleware  
    *
    */
    private static function callRouteMiddlewares(Request $request) : void {
        if (is_countable($routeMiddlewares = self::getRouteMiddlewares())) {
            foreach($routeMiddlewares as $middleware) {
                self::callMiddleware($middleware , $request);
            }
        }
    }

    /*
    *
    * Call middlewares  
    *
    */
    private static function callMiddleware(string $middleware , Request $request) : void {
        $middlewarePath = self::BaseMiddlewareNamespace . $middleware . 'Middleware';
        if(class_exists($middlewarePath)) {
            $middlewareObject = new $middlewarePath;
            $middlewareObject->handle($request);
        }
    }
}