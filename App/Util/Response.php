<?php
namespace App\Util;

class Response {

    /*
    *
    * Returns a valid json response
    *
    */
    public static function returnJSONResponse(array $response) {
        if($response['success']) {
            http_response_code(200);
        }
        else {
            if(isset($response['auth'])) {
                http_response_code(401);
            }
            else {
                http_response_code(400);
            }
        }
        echo json_encode($response);
    }

}