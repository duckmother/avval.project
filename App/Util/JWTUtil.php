<?php
namespace App\Util;

trait JWTUtil {

    /*
    *
    * Set appropriate jwt headers 
    *
    */
    protected function setJWTHeaders() {
        header("Access-Control-Allow-Origin: http://" . $_SERVER['SERVER_NAME'].'/');
        header("Content-Type: application/json; charset=UTF-8");
        header("Access-Control-Allow-Methods: POST, PUT, DELETE");
        header("Access-Control-Max-Age: 3600");
        header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");
    }
}