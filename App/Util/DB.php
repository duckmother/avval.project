<?php
namespace App\Util;

use stdClass;
use mysqli;

final class DB {
    private static $credentials;
    private static $connection;

    private function __construct() {
        //you can not instanciate this class
    }

    /*
    *
    * Connects to database and returns the connection handler object 
    *
    */
    private static function connect() {
        self::setCredentials();

        self::$connection = new mysqli(self::$credentials->host, 
        self::$credentials->user, self::$credentials->pass, 
        self::$credentials->name);

        if(empty(self::$connection->connect_errno)) {
            self::$connection->set_charset('utf8');
            self::$connection->query("SET NAMES 'utf8'");
        }
        return self::$connection;
    }

    /*
    *
    * set credentials that are given in config/database.php
    *
    */
    private static function setCredentials() {
        self::$credentials = new stdClass;
        self::$credentials->host = DB_HOST;
        self::$credentials->name = DB_NAME;
        self::$credentials->user = DB_USER;
        self::$credentials->pass = DB_PASS;
    }

    /*
    *
    * Getter function for the connection handler object
    *
    */
    public static function getDB() {
        if (is_null(self::$connection)) {
            return self::connect();
        }
        return self::$connection;
    }

    /*
    *
    * Gets connection handler object errors
    *
    */
    public function getConnectionError() {
        if(!is_null(self::$connection)) {
            if(isset(self::$connection->connect_errno)) {
                return self::$connection->connect_error;
            }
        }
    }
}