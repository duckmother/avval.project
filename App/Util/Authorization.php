<?php
namespace App\Util;

trait Authorization {

    /*
    *
    * Redirect functionality
    *
    */
    protected function redirect(string $uri) : void {
        header("Location: " . $uri);
        die();
    }
}