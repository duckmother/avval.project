<?php
namespace App\Util;

use stdClass;

trait RouteUtil {

    /*
    *
    * Gets Current URI
    *
    */
    public static function getCurrentRoute() : string {
        return strtok($_SERVER['REQUEST_URI'] , '?');
    }

    /*
    *
    * Checks if the given route is a valid route of routes/web.php
    *
    */
    protected static function isRoute(string $route) : bool {
        foreach(self::$routes as $croute) {
            if ($croute['route'] == $route) {
                return true;
            }
        }
        return false;
    }

    /*
    *
    * Checks if the given route is a valid api route of routes/api.php
    *
    */
    protected static function isAPIRoute(string $route) : bool {
        foreach(self::$apiRoutes as $croute) {
            if ($croute['route'] == $route) {
                return true;
            }
        }
        return false;
    }

    /*
    *
    * Gets credentials of a route
    *
    */
    private static function getRouteCredentials(string $route , array $routes , string $method) : object {
        $credentials = new stdClass;
        foreach ($routes as $croute) {
            if ($croute['route'] == $route && $croute['method'] == $method) {
                $credentials = (object)$croute;
            }
        }
        return $credentials;
    }

    /*
    *
    * Gets a list of middlewares from router file
    *
    */
    private static function getRouteMiddlewares() : array {
        if (isset(self::$routeCredentials->middleware)) {
            return explode('|' , self::$routeCredentials->middleware);
        }
        return [];
    }

    /*
    *
    * Sends a 404 header
    *
    */
    protected static function send404() {
        header('HTTP/1.0 404 Not Found', true, 404);
        echo '404';
        die();
    }

    /*
    *
    * Sends a 403 header
    *
    */
    protected static function send403() {
        header('HTTP/1.0 403 Forbidden' , true , 403 );
        echo '403';
        die();
    }
}