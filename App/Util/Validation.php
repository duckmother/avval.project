<?php
namespace App\Util;

use App\Core\Request;

trait Validation {
    private static $errors;

    /*
    *
    * Runs an appropriate validation for each input
    *
    */
    protected function validator(Request $request , array $credentials) {
        foreach($credentials as $inputName => $rules) {
            $this->callValidation($this->getObjectiveRequest($request) , $inputName , $rules);
        }
        if(empty(self::$errors)) {
            return true;
        }
        return false;
    }

    /*
    *
    * Runs an appropriate validation for each rule and sends errors
    *
    */
    private function callValidation(Request $request , string $inputName , string $rules) {
        $rules = explode('|' , $rules);
        foreach($rules as $rule) {
            if($pos = strpos($rule , 'min') !== false && strpos($rule , 'min') >= 0) {
                $min = substr($rule, $pos + 3);
                $this->checkMin($request , $inputName , intval($min));
            }
            elseif($pos = strpos($rule , 'max') !== false && strpos($rule , 'max') >= 0) {
                $max = substr($rule, $pos + 3);
                $this->checkMax($request , $inputName , intval($max));
            }
            else {
                switch($rule) {
                    case 'required':
                        $this->checkRequired($request , $inputName);
                        break;
                    case 'integer':
                        $this->isInteger($request , $inputName);
                        break;
                    case 'string':
                        $this->isString($request , $inputName);
                        break;
                    case 'mail':
                        $this->isValidEmail($request , $inputName);
                        break;
                    default:
                        $this->getAppropriateError('unknown_rule' , $inputName);
                }
            }
        }
    }

    /*
    *
    * Check required rule and sends errors if needed
    *
    */
    private function checkRequired(Request $request , string $inputName) {
        if(!is_null($request->$inputName) && !empty($request->$inputName)) {
            return true;
        }
        $this->getAppropriateError('required' , $inputName);
        return false;
    }

    /*
    *
    * Check Min rule and sends errors if needed
    *
    */
    private function checkMin(Request $request , string $inputName , int $min) {
        if(!is_null($request->$inputName) && strlen($request->$inputName) >= $min) {
            return true;
        }
        $this->getAppropriateError('min' , $inputName , $min);
        return false;
    }

    /*
    *
    * Check Max rule and sends errors if needed
    *
    */
    private function checkMax(Request $request , string $inputName , int $max) {
        if(!is_null($request->$inputName) && strlen($request->$inputName) <= $max) {
            return true;
        }
        $this->getAppropriateError('max' , $inputName , $max);
        return false;
    }

    /*
    *
    * Check if input is integer and sends errors if needed
    *
    */
    private function isInteger(Request $request , string $inputName) {
        if(!is_null($request->$inputName) && is_numeric($request->$inputName)) {
            return true;
        }
        $this->getAppropriateError('integer' , $inputName);
        return false;
    }

    /*
    *
    * Check if input is string and sends errors if needed
    *
    */
    private function isString(Request $request , string $inputName) {
        if(!is_null($request->$inputName) && is_string($request->$inputName)) {
            return true;
        }
        $this->getAppropriateError('string' , $inputName);
        return false;
    }

    /*
    *
    * Check if input is valid email and sends errors if needed
    *
    */
    private function isValidEmail(Request $request , string $inputName) {
        if(!is_null($request->$inputName) && filter_var($request->$inputName , FILTER_VALIDATE_EMAIL)) {
            return true;
        }
        $this->getAppropriateError('mail' , $inputName);
        return false;
    }

    /*
    *
    * Sets validation errors
    *
    */
    private function getAppropriateError(string $rule , string $inputName , $limit = null) {
        global $errors;
        $error = vsprintf($errors[$rule] , [$inputName , $limit]) ?? 'unknown';
        self::$errors[] = $error;
    }

    /*
    *
    * Gets validation errors
    *
    */
    protected function errors() {
        return self::$errors;
    }

    /*
    *
    * Converts request to object
    *
    */
    private function getObjectiveRequest(Request $request) {
        if(!is_object($request)) {
            $request = json_decode($request);
        }
        return $request;
    }
}