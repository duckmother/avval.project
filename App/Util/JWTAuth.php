<?php
namespace App\Util;

use App\Models\User;
use Firebase\JWT\JWT;
use stdClass;
use Exception;

trait JWTAuth {
    /*
    *
    * Checks if a user does exist or not. if yes, returns user object
    *
    */
    private function isValidUser(string $email , string $password) : object {
        $user = new User();
        $cUser = $user->where([
            'email'    => $email,
        ]);

        $cUser = $user->getFirst($cUser);

        if(!empty((array)$cUser)) {
            if(password_verify($password , $cUser->password)) {
                return $cUser;
            }    
        }
        return new stdClass;
    }

    /*
    *
    * Creates a valid JWT token to authenticate users
    *
    */
    private function createToken(object $user) : string {
        $token = array(
                "iss" => ISS,
                "aud" => AUD,
                "iat" => IAT,
                "nbf" => NBF,
                "data" => array(
                    "id"         => $user->id,
                    "name"       => $user->name,
                    "phone"      => $user->phone,
                    "email"      => $user->email,
                    'ip'         => $user->ip,
                    'created_at' => $user->created_at
                )
        );
        return JWT::encode($token , KEY);
    }

    /*
    *
    * Checks if a given token is valid or not
    *
    */
    private function isValidToken(string $token) : bool {
        try {
            $decoded = JWT::decode($token , KEY , ['HS256']);
            return true;
        } catch (Exception $e) {
            return false;
        }
    }

    /*
    *
    * Gets user data with a given valid token
    *
    */
    private function getUserWithToken(string $token) : object {
        try {
            $decoded = JWT::decode($token , KEY , ['HS256']);
            return (object)$decoded->data;
        } catch (Exception $e) {
            return new stdClass;
        }
    }
}