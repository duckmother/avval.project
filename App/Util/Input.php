<?php
namespace App\Util;

class Input {

    /*
    *
    * Sanitizes strings
    *
    */
    public static function sanitize(string $input) : string {
        $input = trim($input);
        $input = stripslashes($input);
        return htmlspecialchars($input);
    }
}