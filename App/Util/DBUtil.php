<?php
namespace App\Util;

use stdClass;

trait DBUtil {

    /*
    *
    * Converts all associated arrays to object
    *
    */
    protected function AssocToObject(array $records = null) : object {
        $objRecords = new stdClass;
        if(is_countable($records)) {
            foreach($records as $key => $value) {
                if(is_array($value)) {
                    $value = $this->AssocToObject($value);
                }
                $objRecords->{$key} = $value;
            }
        }
        return $objRecords;
    }

    /*
    *
    * Gets first record of each result object
    *
    */
    public function getFirst(stdClass $results) {
        return $results->{'0'};
    }
}