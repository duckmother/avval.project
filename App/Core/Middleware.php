<?php
namespace App\Core;

use App\Core\Request;
use App\Util\Authorization;
use Monolog\Logger;
use Monolog\Handler\StreamHandler;

abstract class Middleware {
    use Authorization;

    protected $logger;

    /*
    *
    Constructor
    *
    */
    public function __construct()
    {
        $this->logger = new Logger('avval');
        $this->logger->pushHandler(new StreamHandler(LOG_PATH. 'log.log', Logger::DEBUG));
    }

    /*
    *
    Implement this method to define middleware's behaviour
    *
    */
    public abstract function handle(Request $request);
}