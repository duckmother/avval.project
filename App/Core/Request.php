<?php
namespace App\Core;

use App\Util\Input;
use stdClass;

class Request {
    public $ip;
    public $uri;
    public $method;
    public $agent;
    public $referer;
    public $params;

    /*
    *
    Constructor
    *
    */
    public function __construct()
    {
        $this->ip      = $_SERVER['REMOTE_ADDR'];
        $this->uri     = $_SERVER['REQUEST_URI'];
        $this->method  = $_SERVER['REQUEST_METHOD'];
        $this->agent   = $_SERVER['HTTP_USER_AGENT'];
        $this->referer = $_SERVER['HTTP_REFERER'] ?? NULL;

        if(SANITIZE_ALL) {
            $this->sanitizeRequest();
        }
        else {
            $this->params  = (object)$_REQUEST;
        }
    }

    /*
    *
    Check if a request parameter does exist
    *
    */
    public function isParam(string $paramName) {
        if(array_key_exists($paramName , $_REQUEST)) {
            return $_REQUEST[$paramName];
        }
        return false;
    }

    /*
    *
    Gets a parameter i exists
    *
    */
    public function input(string $inputName) {
        if ($param = $this->isParam($inputName)) {
            return $_REQUEST[$inputName];
        }
        return NULL;
    }

    /*
    *
    gives a Request class property if mapped parameter does exist
    *
    */
    public function __get($inputName)
    {
        if($param = $this->isParam($inputName)) {
            return $this->$inputName = $param;
        }
        return NULL;
    }

    /*
    *
    sanitize all parameters of request
    *
    */
    private function sanitizeRequest() {
        $this->params = new stdClass;
        foreach($_REQUEST as $key => $value) {
            $this->params->$key = Input::sanitize($value);
        }
    }
}