<?php
namespace App\Core;

use App\Util\DB;
use stdClass;
use App\Util\DBUtil;

class Model {
    use DBUtil;

    protected static $table;
    protected static $primary = 'id';

    /*
    *
    handling INSERT queries
    *
    */
    public function create(array $credentials) : bool {
        $query = 'INSERT INTO ' . static::$table . ' ( ';
        foreach($credentials as $key => $value) {
            $query .= "$key, ";
        }
        $query = rtrim($query , ', ') . ') VALUES (';
        foreach($credentials as $key => $value) {
            $query .= "'$value', ";
        }
        $query = rtrim($query , ', ') . ')';
        if(empty((array)$this->query($query))) {
            return false;
        }
        return true;
    }

    /*
    *
    handling SELECT * queries
    *
    */
    public function all() : object{
        $query = 'SELECT * FROM ' . static::$table;
        return $this->query($query);
    }

    /*
    *
    handling SELECT LIMIT 1 queries
    *
    */
    public function find(int $key) : object{
        $query = 'SELECT * FROM ' . static::$table . ' WHERE ' . static::$primary . "='$key' LIMIT 1";
        $result = $this->query($query);
        if(empty((array)$result)) {
            return new stdClass;
        }
        return $result->{'0'};
    }

    /*
    *
    handling SELECT WHERE queries
    *
    */
    public function where(array $credentials) : object{
        $query = 'SELECT * FROM ' . static::$table . ' WHERE ';
        foreach($credentials as $key => $value) {
            $query .= "$key='$value' AND ";
        }
        $query = rtrim($query, ' AND ');
        return $this->query($query);
    }

    /*
    *
    handling UPDATE queries
    *
    */
    public function update(array $credentials , array $whereCredentials = array()) : bool {
        $query = 'UPDATE ' . static::$table . ' SET ';
        foreach($credentials as $key => $value) {
            $query .= "$key='$value', ";
        }
        $query = rtrim($query , ', ');
        $query .= ' WHERE ';
        foreach($whereCredentials as $key => $value) {
            $query .= "$key='$value' AND";
        }
        $query = rtrim($query , ' AND');
        if(empty((array)$this->query($query))) {
            return false;
        }
        return true;
    }

    /*
    *
    handling DELETE WHERE queries
    *
    */
    public function deleteWhere(array $credentials) : bool {
        $query = 'DELETE FROM ' . static::$table . ' WHERE ';
        foreach($credentials as $key => $value) {
            $query .= "$key='$value' AND";
        }
        $query = rtrim(' AND');
        if(empty((array)$this->query($query))) {
            return false;
        }
        return true;
    }

    /*
    *
    handling DELETE with id queries
    *
    */
    public function delete(int $key) : bool {
        $query = 'DELETE FROM ' . static::$table . ' WHERE ' . static::$primary . "='$key'";
        if(empty((array)$this->query($query))) {
            return false;
        }
        return true;
    }

    /*
    *
    handling all types of queries
    *
    */
    public function query(string $query) : object {
        if($results = DB::getDB()->query($query)) {
            if(is_bool($results)) {
                return (object)$results;
            }

            $records = $results->fetch_all(MYSQLI_ASSOC);
            if(!is_null($records)) {
                return (object)$this->AssocToObject($records);
            }
        }
        return new stdClass;
    }
}