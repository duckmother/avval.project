<?php 
namespace App\Core;

use BadMethodCallException;
use App\Util\Authorization;
use App\Util\Validation;
use Monolog\Logger;
use Monolog\Handler\StreamHandler;

abstract class Controller {
    use Authorization , Validation;

    public $middlewares;
    protected $logger;

    /*
    *
    Constructor
    *
    */
    public function __construct()
    {
        foreach ($this->middleware() as $middleware) {
            $this->middlewares[] = $middleware;
        }
        $this->logger = new Logger('avval');
        $this->logger->pushHandler(new StreamHandler(LOG_PATH. 'log.log', Logger::DEBUG));
    }

    /*
    *
    Implement this method to have a middleware container on whole controller
    *
    */
    protected abstract function middleware() : array;

    /*
    *
    throwing exception if method was not exist
    *
    */
    public function __call($methodName, $params)
    {
        throw new BadMethodCallException(sprintf('Method %s from class %s does not exists.'
         , $methodName , static::class));
    }
}