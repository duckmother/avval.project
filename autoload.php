<?php

function autoloader($className) {
    $classPath = $className . '.php';
    if(file_exists($classPath) & is_readable($classPath)) {
        include $classPath;
    }
}

spl_autoload_register('autoloader');