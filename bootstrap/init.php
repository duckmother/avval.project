<?php
define('BASE_PATH' , __DIR__ . DIRECTORY_SEPARATOR . '../');
define('VIEW_PATH' , BASE_PATH . 'views/');
define('ROUTE_PATH' , BASE_PATH . 'routes/');
define('LOG_PATH' , BASE_PATH . 'logs/');

define('ROUTE_DELIMITER' , '@');
