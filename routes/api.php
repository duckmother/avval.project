<?php
return [
    [
        'route'  => '/api/user',
        'method' => 'get',
        'dest'   => 'AuthController@getAuthUser'
    ],
    [
        'route'  => '/api/register',
        'method' => 'post',
        'dest'   => 'AuthController@register'
    ],
    [
        'route'  => '/api/login',
        'method' => 'post',
        'dest'   => 'AuthController@login'
    ],

    [
        'route'  => '/api/post',
        'method' => 'get',
        'dest'   => 'PostController@show'
    ],
    [
        'route'      => '/api/post',
        'method'     => 'post',
        'dest'       => 'PostController@store',
        'middleware' => 'JWTAuth'
    ],
    [
        'route'      => '/api/post',
        'method'     => 'put',
        'dest'       => 'PostController@update',
        'middleware' => 'JWTAuth'
    ],
    [
        'route'      => '/api/post',
        'method'     => 'delete',
        'dest'       => 'PostController@destroy',
        'middleware' => 'JWTAuth'
    ],
    [
        'route'      => '/api/comment',
        'method'     => 'get',
        'dest'       => 'CommentController@index',
    ],
    [
        'route'      => '/api/comment',
        'method'     => 'post',
        'dest'       => 'CommentController@store',
        'middleware' => 'JWTAuth'
    ],
    [
        'route'      => '/api/comment',
        'method'     => 'delete',
        'dest'       => 'CommentController@destroy',
        'middleware' => 'JWTAuth'
    ]
];