<?php

return [
    [
        'route'  => '/',
        'method' => 'get',
        'dest'   => 'HomeController@index'
    ]
];