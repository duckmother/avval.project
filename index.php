<?php

error_reporting(E_ALL);
ini_set('display_error' , 1);

require_once 'autoload.php';
require __DIR__ . '/vendor/autoload.php';

require_once 'config/app.php';
require_once 'config/database.php';
require_once 'bootstrap/init.php';
require_once 'config/jwt.php';
require_once 'lang/errors.php';


\App\Services\Router\Router::bootstrap();